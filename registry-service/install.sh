#!/bin/sh
rm -R build
./gradlew clean build jar -x test
sudo docker rmi registry-service:v1
sudo docker build -t registry-service:v1 .