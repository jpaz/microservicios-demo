package alpha.microservices.commonservice.clients.feign;


import alpha.microservices.commonservice.models.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(name = "product-service")
@RequestMapping(value = "/products")
public interface ProductClient {
	@GetMapping
	List<Product> findAll();

	@GetMapping(value = "/{id}")
	Product findById(@PathVariable(value = "id") Long id);

	@PostMapping
	Product save(@RequestBody Product product);

	@PutMapping(value = "/{id}")
	Product update(@PathVariable(value = "id") Long id, @RequestBody Product product);

	@DeleteMapping(value = "/{id}")
	Product delete(@PathVariable(value = "id") Long id);
}
