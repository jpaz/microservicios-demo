package alpha.microservices.commonservice.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
	private Long id;
	private String username;
	private String password;
	private String email;
	private String firstname;
	private String lastname;
	private Boolean enable;
	private Integer loginAttempts;
	private List<Role> roles;
}
