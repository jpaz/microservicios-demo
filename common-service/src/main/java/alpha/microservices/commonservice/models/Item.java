package alpha.microservices.commonservice.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Item {
	private Product product;
	private Integer quantity;

	public Double getTotal() {
		return product.getPrice() * quantity;
	}
}
