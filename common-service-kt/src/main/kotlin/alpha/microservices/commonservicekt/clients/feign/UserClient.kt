package alpha.microservices.commonservicekt.clients.feign

import alpha.microservices.commonservicekt.models.User
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.*

@FeignClient(name = "user-service")
@RequestMapping(value = ["/users"])
interface UserClient {
    @GetMapping
    fun findAll(): List<User>

    @GetMapping(value = ["/{id}"])
    fun findById(@PathVariable(value = "id") id: Long): User?

    @GetMapping(value = ["/search/find-by-username"])
    fun findByUsername(@RequestParam(value = "username") username: String): User?

    @PostMapping
    fun save(@RequestBody user: User): User?

    @PutMapping(value = ["/{id}"])
    fun update(@RequestBody user: User, @PathVariable(value = "id") id: Long): User?

    @DeleteMapping(value = ["/{id}"])
    fun delete(@PathVariable(value = "id") id: Long): User?
}