package alpha.microservices.commonservicekt.models

data class Role(
        val id: Long,
        val name: String
)