package alpha.microservices.commonservicekt.models

import java.util.Date

data class Product(
        val id: Long,
        val name: String,
        val price: Double,
        val createAt: Date
)