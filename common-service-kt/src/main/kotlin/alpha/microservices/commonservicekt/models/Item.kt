package alpha.microservices.commonservicekt.models

data class Item(
        val product: Product,
        val quantity: Int
) {
    val total = this.product.price * quantity
}