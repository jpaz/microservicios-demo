package alpha.microservices.commonservicekt.models

data class User(
        val id: Long?,
        var username: String,
        var password: String,
        var email: String,
        var firstname: String,
        var lastname: String,
        var enable: Boolean,
        var loginAttempts: Int,
        var roles: List<Role>
)