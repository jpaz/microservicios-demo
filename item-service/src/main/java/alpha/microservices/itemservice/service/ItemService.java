package alpha.microservices.itemservice.service;

import alpha.microservices.commonservice.models.Item;
import alpha.microservices.commonservice.models.Product;

import java.util.List;

public interface ItemService {
	List<Item> findAll();

	Item findById(Long id, Integer quantity);

	Product save(Product product);

	Product update(Product product);

	Product deleteById(Long id);
}
