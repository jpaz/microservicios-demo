package alpha.microservices.itemservice.service.impl;

import alpha.microservices.commonservice.models.Item;
import alpha.microservices.commonservice.models.Product;
import alpha.microservices.itemservice.service.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ItemServiceImpl implements ItemService {

	//	private static final String GET_PRODUCT_BY_ID_URL = "http://localhost:8001/{id}";
	//	private static final String GET_PRODUCTS_URL = "http://localhost:8001/";
	private static final String PRODUCTS_URL_BASE = "http://product-service/items";
	private static final String PRODUCT_BY_ID_URL = PRODUCTS_URL_BASE + "/{id}";

	private final RestTemplate restTemplate;

	@Override
	public List<Item> findAll() {
		Product[] response = restTemplate.getForObject(PRODUCTS_URL_BASE, Product[].class);
		if (response != null) {
			List<Product> products = Arrays.asList(response);
			return products.stream()
					.map(p -> new Item(p, 1))
					.collect(Collectors.toList());
		}
		return null;
	}

	@Override
	public Item findById(Long id, Integer quantity) {
		Product product = restTemplate.getForObject(PRODUCT_BY_ID_URL, Product.class, id);
		if (product != null) {
			return Item.builder()
					.quantity(quantity)
					.product(product)
					.build();
		}
		return null;
	}

	@Override
	public Product save(Product product) {
		return restTemplate.postForObject(PRODUCTS_URL_BASE, product, Product.class);
	}

	@Override
	public Product update(Product product) {
		return restTemplate.exchange(
				PRODUCT_BY_ID_URL,
				HttpMethod.PUT,
				new HttpEntity<>(product),
				Product.class,
				product.getId()
		).getBody();
	}

	@Override
	public Product deleteById(Long id) {
		return restTemplate.exchange(
				PRODUCT_BY_ID_URL,
				HttpMethod.DELETE,
				null,
				Product.class,
				id
		).getBody();
	}
}
