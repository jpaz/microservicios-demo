package alpha.microservices.itemservice.service.impl;

import alpha.microservices.commonservice.models.Item;
import alpha.microservices.commonservice.models.Product;
import alpha.microservices.commonservice.clients.feign.ProductClient;
import alpha.microservices.itemservice.service.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ItemServiceFeign implements ItemService {

	private final ProductClient productClient;

	@Override
	public List<Item> findAll() {
		List<Product> products = productClient.findAll();
		if (products != null && !products.isEmpty()) {
			return products.stream()
					.map(p -> new Item(p, 1))
					.collect(Collectors.toList());
		}
		return null;
	}

	@Override
	public Item findById(Long id, Integer quantity) {
		Product product = productClient.findById(id);
		if (product != null) {
			return Item.builder()
					.quantity(quantity)
					.product(product)
					.build();
		}
		return null;
	}

	@Override
	public Product save(Product product) {
		return productClient.save(product);
	}

	@Override
	public Product update(Product product) {
		return productClient.update(product.getId(), product);
	}

	@Override
	public Product deleteById(Long id) {
		return productClient.delete(id);
	}
}
