package alpha.microservices.itemservice.controller;

import alpha.microservices.commonservice.models.Item;
import alpha.microservices.commonservice.models.Product;
import alpha.microservices.itemservice.service.ItemService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RefreshScope
@RestController
@RequestMapping(value = "/items")
public class ItemController {

	private final ItemService itemService;

	public ItemController(@Qualifier(value = "itemServiceFeign") ItemService itemService) {
		this.itemService = itemService;
	}

	@GetMapping
	public List<Item> findAll() {
		return itemService.findAll();
	}

	@HystrixCommand(fallbackMethod = "alternativeCallback")
	@GetMapping(value = "/{id}")
	public Item findById(@PathVariable(value = "id") Long id, @RequestParam(value = "quantity") Integer quantity) {
		return itemService.findById(id, quantity);
	}

	public Item alternativeCallback(Long id, Integer quantity) {
		Product product = new Product(id, "none", 0D, new Date());
		return new Item(product, quantity);
	}

	@GetMapping(value = "/config")
	public ResponseEntity<Map<String, String>> findConfig(@Value(value = "${application.env}") String env,
														  @Value(value = "${application.description}") String description,
														  @Value(value = "${server.port}") String port) {
		Map<String, String> config = new HashMap<>();
		config.put("env", env);
		config.put("description", description);
		config.put("port", port);
		return ResponseEntity.ok(config);
	}

	@PostMapping
	public Product save(@RequestBody Product product) {
		return itemService.save(product);
	}

	@PutMapping(value = "/{id}")
	public Product update(@PathVariable(value = "id") Long id, @RequestBody Product product) {
		product.setId(id);
		return itemService.update(product);
	}

	@DeleteMapping(value = "/{id}")
	public Product delete(@PathVariable(value = "id") Long id) {
		return itemService.deleteById(id);
	}
}
