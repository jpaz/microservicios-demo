package alpha.microservices.oauthservice.services.impl

import alpha.microservices.commonservicekt.clients.feign.UserClient
import alpha.microservices.commonservicekt.models.User
import alpha.microservices.oauthservice.services.UserService
import brave.Tracer
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import java.util.logging.Logger
import org.springframework.security.core.userdetails.User as UserDetail

@Suppress("SpringJavaInjectionPointsAutowiringInspection")
@Service
class UserServiceImpl(
        private val logger: Logger = Logger.getLogger(UserServiceImpl::class.toString()),
        private val userClient: UserClient,
        private val tracer: Tracer
) : UserDetailsService, UserService {

    override fun findByUsername(username: String): User? {
        return try {
            userClient.findByUsername(username)
        } catch (e: Exception) {
            logger.severe("Fail to find user $username: ${e.message}")
            null
        }
    }

    override fun update(user: User): User? {
        return try {
            user.id?.let { userClient.update(user, it) }
        } catch (e: Exception) {
            logger.severe("Fail to update user ${user.username}: ${e.message}")
            null
        }
    }

    override fun loadUserByUsername(username: String): UserDetails {
        try {
            val user: User = userClient.findByUsername(username) ?: throw UsernameNotFoundException("User not found")
            val authorities: List<GrantedAuthority>? = user.roles.map { r -> SimpleGrantedAuthority(r.name) }
            return UserDetail(
                    user.username,
                    user.password,
                    user.enable,
                    true,
                    true,
                    true,
                    authorities
            )
        } catch (e: Exception) {
            val message = "Fail to load user details: ${e.message}"
            logger.warning(message)
            tracer.currentSpan().tag("ERROR", message)
            throw UsernameNotFoundException(message)
        }
    }
}