package alpha.microservices.oauthservice.services

import alpha.microservices.commonservicekt.models.User

interface UserService {
    fun findByUsername(username: String): User?

    fun update(user: User): User?
}