package alpha.microservices.oauthservice.security.event

import alpha.microservices.commonservicekt.models.User
import alpha.microservices.oauthservice.services.UserService
import brave.Tracer
import org.springframework.security.authentication.AuthenticationEventPublisher
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component
import java.util.logging.Logger

@Suppress("SpringJavaInjectionPointsAutowiringInspection")
@Component
class AuthenticationEventHandler(
        private val logger: Logger = Logger.getLogger(AuthenticationEventHandler::class.toString()),
        private val userService: UserService,
        private var tracer: Tracer
) : AuthenticationEventPublisher {

    override fun publishAuthenticationSuccess(authentication: Authentication) {
        val userDetails: UserDetails = authentication.principal as UserDetails
        logger.info("Success logging: ${userDetails.username}")

        try {
            val user: User = userService.findByUsername(authentication.name)
                    ?: throw UsernameNotFoundException("Not user found: ${authentication.name}")

            if (user.loginAttempts > 0) {
                logger.info("Reset login attempts to zero for user ${user.username}")
                user.loginAttempts = 0
                userService.update(user)
            }
        } catch (e: Exception) {
            logger.warning(e.message)
        }
    }

    override fun publishAuthenticationFailure(exception: AuthenticationException, authentication: Authentication) {
        val stringBuilder = StringBuilder()
        var message = "Failed login: ${exception.message}"
        logger.warning(message)
        stringBuilder.append(message)

        try {
            val user: User = userService.findByUsername(authentication.name)
                    ?: throw UsernameNotFoundException("Not user found: ${authentication.name}")

            user.loginAttempts++

            if (user.loginAttempts >= 3) {
                message = "Disable user ${user.username} for reaching maximum login attempt"
                logger.warning(message)
                stringBuilder.append(message)
                user.enable = false
            }

            userService.update(user)
        } catch (e: Exception) {
            logger.warning(e.message)
            stringBuilder.append(e.message)
            tracer.currentSpan().tag("error", stringBuilder.toString())
        }
    }
}