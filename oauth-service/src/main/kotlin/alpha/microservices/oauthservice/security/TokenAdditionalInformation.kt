package alpha.microservices.oauthservice.security

import alpha.microservices.commonservicekt.models.User
import alpha.microservices.oauthservice.services.UserService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.token.TokenEnhancer
import org.springframework.stereotype.Component

@Component
class TokenAdditionalInformation(private val userService: UserService) : TokenEnhancer {
    override fun enhance(accessToken: OAuth2AccessToken, authentication: OAuth2Authentication): OAuth2AccessToken {
        val user: User = userService.findByUsername(authentication.name)
                ?: throw UsernameNotFoundException("User not found")

        (accessToken as DefaultOAuth2AccessToken).additionalInformation = mapOf(
                "firstname" to user.firstname,
                "lastname" to user.lastname,
                "email" to user.email
        )

        return accessToken
    }
}