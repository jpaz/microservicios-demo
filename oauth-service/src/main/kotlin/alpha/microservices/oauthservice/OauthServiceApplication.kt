package alpha.microservices.oauthservice

import alpha.microservices.commonservicekt.clients.feign.UserClient
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@EnableEurekaClient
@EnableFeignClients(clients = [UserClient::class])
@SpringBootApplication(exclude = [DataSourceAutoConfiguration::class])
class OauthServiceApplication(private val bCryptPasswordEncoder: BCryptPasswordEncoder) : CommandLineRunner {
    override fun run(vararg args: String?) {
        val passwords = arrayOf("1234", "root", "p433w0rd")
        passwords.forEach { password ->
            println("$password -> encrypt -> ${bCryptPasswordEncoder.encode(password)}")
        }
    }
}

fun main(args: Array<String>) {
    runApplication<OauthServiceApplication>(*args)
}
