rootProject.name = "oauth-service"

includeBuild("../common-service-kt") {
    dependencySubstitution {
        substitute(module("alpha.microservices:common-service-kt:0.0.1-SNAPSHOT")).with(project(":"))
    }
}