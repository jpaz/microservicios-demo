#!/bin/sh
rm -R build
./gradlew clean build jar -x test
sudo docker rmi zuul-service:v1
sudo docker build -t zuul-service:v1 .