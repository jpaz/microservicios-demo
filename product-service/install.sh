#!/bin/sh
rm -R build
./gradlew clean build jar -x test
sudo docker rmi product-service:v1
sudo docker build -t product-service:v1 .