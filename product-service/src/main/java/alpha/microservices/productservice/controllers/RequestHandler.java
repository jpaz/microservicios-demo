package alpha.microservices.productservice.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class RequestHandler implements HandlerInterceptor {

	@Value("${server.port}")
	private String port;
	@Value("${spring.application.name}")
	private String appName;

	private final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		System.out.printf("%s --- [%s] %s: Calling %s:%s \n", format.format(new Date()), request.getMethod(), appName, port, request.getRequestURI());
		return true;
	}
}
