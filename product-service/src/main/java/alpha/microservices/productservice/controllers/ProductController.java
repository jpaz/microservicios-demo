package alpha.microservices.productservice.controllers;

import alpha.microservices.productservice.models.entity.Product;
import alpha.microservices.productservice.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@RestController
@RequestMapping(value = "/products")
@RequiredArgsConstructor
public class ProductController implements WebMvcConfigurer {

	private final ProductService productService;
	private final RequestHandler requestHandler;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(requestHandler);
	}

	@GetMapping
	public List<Product> findAll() {
		return productService.findAll();
	}

	@GetMapping(value = "/{id}")
	public Product findById(@PathVariable("id") Long id) {
		return productService.findById(id);
	}

	@PostMapping
	public Product save(@RequestBody Product product) {
		return productService.save(product);
	}

	@PutMapping(value = "/{id}")
	public Product update(@PathVariable(value = "id") Long id, @RequestBody Product product) {
		product.setId(id);
		return productService.update(product);
	}

	@DeleteMapping(value = "/{id}")
	public Product delete(@PathVariable(value = "id") Long id) {
		return productService.deleteById(id);
	}
}
