package alpha.microservices.productservice.service.impl;

import alpha.microservices.productservice.models.entity.Product;
import alpha.microservices.productservice.repository.ProductRepository;
import alpha.microservices.productservice.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

	private final ProductRepository productRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Product> findAll() {
		return productRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Product findById(Long id) {
		return productRepository.findById(id).orElse(null);
	}

	@Override
	public Product save(Product product) {
		return productRepository.save(product);
	}

	@Override
	public Product update(Product product) {
		if (product != null && productRepository.existsById(product.getId())) {
			return save(product);
		}
		return null;
	}

	@Override
	public Product deleteById(Long id) {
		Product product = findById(id);
		if (product != null) {
			productRepository.delete(product);
			return product;
		}
		return null;
	}
}
