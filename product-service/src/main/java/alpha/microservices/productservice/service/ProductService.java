package alpha.microservices.productservice.service;

import alpha.microservices.productservice.models.entity.Product;

import java.util.List;

public interface ProductService {
	List<Product> findAll();

	Product findById(Long id);

	Product save(Product product);

	Product update(Product product);

	Product deleteById(Long id);
}
