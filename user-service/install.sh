#!/bin/sh
rm -R build
./gradlew clean build jar -x test
sudo docker rmi user-service:v1
sudo docker build -t user-service:v1 .