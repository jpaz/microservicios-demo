package alpha.microservices.userservice.repository;

import alpha.microservices.userservice.models.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource
public interface UserRepository extends JpaRepository<User, Long> {

	@RestResource(path = "find-by-username")
	User findByUsername(@Param("username") String username);

	@RestResource(path = "find-by-username-and-email")
	User findByUsernameAndEmail(@Param("username") String username, @Param("email") String email);
}
