package alpha.microservices.userservice.models.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.checkerframework.common.aliasing.qual.Unique;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "users")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Unique
	@NotEmpty(message = "The username cannot be empty")
	@Length(message = "Username name must be at least 6 characters long", min = 4, max = 20)
	private String username;

	@NotEmpty(message = "The password cannot be empty")
	@Pattern(message = "Invalid password format", regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$")
	private String password;

	@Email(message = "Invalid email format")
	@NotEmpty(message = "The email cannot be empty")
	private String email;

	@NotEmpty(message = "The firstname cannot be empty")
	@Length(message = "First name must be at least 2 characters long", min = 2, max = 60)
	private String firstname;

	@NotEmpty(message = "The lastname cannot be empty")
	@Length(message = "Last name must be at least 2 characters long", min = 2, max = 60)
	private String lastname;

	@NotNull(message = "Enable cannot be null")
	private Boolean enable;

	@Column(name = "login_attempts")
	private Integer loginAttempts;

	@NotNull(message = "User roles cannot be null")
	@Size(message = "User needs at least 1 role", min = 1)
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_role",
			joinColumns = @JoinColumn(name = "user_id"),
			inverseJoinColumns = @JoinColumn(name = "role_id"),
			uniqueConstraints = {@UniqueConstraint(columnNames = {"user_id", "role_id"})}
	)
	private List<Role> roles;

	@PrePersist
	private void prePersist() {
		if (loginAttempts == null) {
			loginAttempts = 0;
		}
	}
}
