INSERT INTO roles (id, name) VALUES (0, 'ROLE_ROOT')

INSERT INTO roles (id, name) VALUES (1, 'ROLE_USER')

INSERT INTO users (id, username, password, email, firstname, lastname, enable, login_attempts) VALUES (0, 'root', '$2a$10$eR7NeGJbURP.Wvm0peJlVuijSEBj3szuy4l8VuvyiFLh1vN/sSux6', 'root@email.com', 'root', 'root', TRUE, 0)

INSERT INTO user_role (user_id, role_id) VALUES (0, 0)
